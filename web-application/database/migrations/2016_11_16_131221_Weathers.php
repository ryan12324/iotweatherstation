<?php

use duxet\Rethinkdb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Weathers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weathers', function (Blueprint $table) {
            $table->increments('id');
            $table->float('temperature', 5, 2);
            $table->float('windspeed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('weathers');
    }
}