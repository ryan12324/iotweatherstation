#!/usr/bin/env bash
cd web-application;
composer update;
npm install;
cd ..;
cd api-application;
npm install;
cd ..;

docker-compose up -d;

echo "create a database called iotweatherstation in rethink db and run the laravel migrations";
echo "also create a /etc/hosts entry";
echo "127.0.0.1 iot-weatherstation.local";